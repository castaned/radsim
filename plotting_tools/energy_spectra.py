import pandas as pd
import matplotlib.pyplot as plt
import math
import pylab
import sys
from matplotlib.font_manager import FontProperties

sys.setrecursionlimit(1500)

geometry_location="C:/Users/Alfredo/Documents/Github/radsim/geometries/v_3_31_4_2"

geomTag="v3_31_4_2_"

stations = ["21","22","23","24","25","26","27","28","30","31","32","33","34","35","36","37","38"]

colnames=["e_bin_left","e_bin_right","dfluxdE","error"]

LumiTag = "1.5x10E34/"

for station in stations:
 if station!="34" and station!="35":
       stationname=""
       volume = 0
       if station == "22":
        stationname = "ME12"
        volume = 16062814.0
       if station == "23":
        stationname = "ME13"
        volume = 19054620.4
       if station == "24":
        stationname = "ME21"
        volume = 13476517.436
       if station == "25":
        stationname = "ME22"
        volume = 53459478.023
       if station == "26":
        stationname = "ME31"
        volume = 13075784.527
       if station == "27":
        stationname = "ME41"
        volume = 11664782.8
       if station == "28":
        stationname = "ME42"
        volume = 53276162.2
       if station == "30":
        stationname = "MB1_0"
        volume = 22195182.45
       if station == "31":
        stationname = "MB1_1"
        volume = 41714350.0
       if station == "32":
        stationname = "MB1_2"
        volume = 41264341.98
       if station == "33":
        stationname = "MB2_0"
        volume = 14955369.62
        stationname2 = "MB3_0"
        volume2 = 13019614.47
       if station == "34":
        stationname = "MB2_1"
        volume = 28107609.56
        stationname2 = "MB3_1"
        volume2 = 24469488.19
       if station == "35":
        stationname = "MB2_2"
        volume = 26940878.6
        stationname2 = "MB3_2"
        volume2 = 23453773.58
       if station == "36":
        stationname = "MB4_0"
        volume = 6223652.126
       if station == "37":
        stationname = "MB4_1"
        volume = 11696934.85
       if station == "38":
        stationname = "MB4_2"
        volume = 11211401.7


       data_neut = pd.read_table(geometry_location+"/raw_data/"+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=list(range(0,2))+list(range(264,266)),nrows=360,names=colnames)
       data_ph = pd.read_table(geometry_location+"/raw_data/"+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=range(0,370),nrows=100,names=colnames)
       data_epem = pd.read_table(geometry_location+"/raw_data/"+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=range(0,474),nrows=100,names=colnames)

       data_neut["bin_center"] = (data_neut["e_bin_right"] + data_neut["e_bin_left"])/2.
       data_neut["dfluxdEN"] = data_neut["dfluxdE"]/volume
       data_neut["EdfluxdE"] = (data_neut["bin_center"]*data_neut["dfluxdE"])/volume
       data_neut["errdfluxdE"]  = ((data_neut["error"]/100.0)*data_neut["dfluxdEN"])
       data_neut["errEdfluxdE"]  = ((data_neut["error"]/100.0)*data_neut["EdfluxdE"])

       data_ph["bin_center"] = (data_ph["e_bin_right"] + data_ph["e_bin_left"])/2.
       data_ph["dfluxdEN"] = data_ph["dfluxdE"]/volume
       data_ph["EdfluxdE"] = (data_ph["bin_center"]*data_ph["dfluxdE"])/volume
       data_ph["errdfluxdE"] = ((data_ph["error"]/100.0)*data_ph["dfluxdEN"])
       data_ph["errEdfluxdE"]  = ((data_ph["error"]/100.0)*data_ph["EdfluxdE"])

       data_epem["bin_center"] = (data_epem["e_bin_right"] + data_epem["e_bin_left"])/2.
       data_epem["dfluxdEN"] = (data_epem["dfluxdE"])/volume
       data_epem["EdfluxdE"] = (data_epem["bin_center"]*data_epem["dfluxdE"])/volume
       data_epem["errdfluxdE"]  = ((data_epem["error"]/100.0)*data_epem["dfluxdEN"])
       data_epem["errEdfluxdE"]  = ((data_epem["error"]/100.0)*data_epem["EdfluxdE"])


       if station == "33" or station == "34" or station == "35":
            data_neut2 = pd.read_table(geometry_location+"/raw_data/"+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=list(range(0,578))+list(range(839,842)),nrows=360,names=colnames)
            data_ph2   = pd.read_table(geometry_location+"/raw_data/"+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=range(0,946),nrows=100,names=colnames)
            data_epem2 = pd.read_table(geometry_location+"/raw_data/"+geomTag+"usrtrack_"+station+"_tab.lis",delim_whitespace=True,skiprows=range(0,1050),nrows=100,names=colnames)
      
            data_neut2["bin_center"] = (data_neut2["e_bin_right"] + data_neut2["e_bin_left"])/2.
            data_neut2["dfluxdEN"] = data_neut2["dfluxdE"]/volume
            data_neut2["EdfluxdE"] = (data_neut2["bin_center"]*data_neut2["dfluxdE"])/volume
            data_neut2["errdfluxdE"]  = ((data_neut2["error"]/100.0)*data_neut2["dfluxdEN"])
            data_neut2["errEdfluxdE"]  = ((data_neut2["error"]/100.0)*data_neut2["EdfluxdE"])

            data_ph2["bin_center"] = (data_ph2["e_bin_right"] + data_ph2["e_bin_left"])/2.
            data_ph2["dfluxdEN"] = data_ph2["dfluxdE"]/volume
            data_ph2["EdfluxdE"] = (data_ph2["bin_center"]*data_ph2["dfluxdE"])/volume
            data_ph2["errdfluxdE"]  = ((data_ph2["error"]/100.0)*data_ph2["dfluxdEN"])
            data_ph2["errEdfluxdE"]  = ((data_ph2["error"]/100.0)*data_ph2["EdfluxdE"])

            data_epem2["bin_center"] = (data_epem2["e_bin_right"] + data_epem2["e_bin_left"])/2.
            data_epem2["dfluxdEN"] = data_epem2["dfluxdE"]/volume
            data_epem2["EdfluxdE"] = (data_epem2["bin_center"]*data_epem2["dfluxdE"])/volume
            data_epem2["errdfluxdE"]  = ((data_epem2["error"]/100.0)*data_epem2["dfluxdEN"])
            data_epem2["errEdfluxdE"]  = ((data_epem2["error"]/100.0)*data_epem2["EdfluxdE"])
        
        
#       plot_category = (["EdfluxdE","dfluxdE","EdfluxdEL1p5E34","dfluxdEL1p5E34"])
       #plot_category = (["EdfluxdE","EdfluxdEL1p5E34"])
       plot_category = (["dfluxdEN","EdfluxdE"])
      

       # energy saved to data files
    

       Lumi = 800000000*1.5  # luminosity for 1.5x10E34
    
       data_neut["dfluxdEL1p5E34"] = (data_neut["dfluxdE"]/volume)*Lumi
       data_neut["EdfluxdEL1p5E34"] = (data_neut["EdfluxdE"]/volume)*Lumi
       
       data_ph["dfluxdEL1p5E34"] = (data_ph["dfluxdE"]/volume)*Lumi
       data_ph["EdfluxdEL1p5E34"] = (data_ph["EdfluxdE"]/volume)*Lumi

       data_epem["dfluxdEL1p5E34"] = (data_epem["dfluxdE"]/volume)*Lumi
       data_epem["EdfluxdEL1p5E34"] = (data_epem["EdfluxdE"]/volume)*Lumi


       data_neut["errdfluxdEL1p5E34"] = (data_neut["errdfluxdE"]/volume)*Lumi
       data_neut["errEdfluxdEL1p5E34"] = (data_neut["errEdfluxdE"]/volume)*Lumi
       
       data_ph["errdfluxdEL1p5E34"] = (data_ph["errdfluxdE"]/volume)*Lumi
       data_ph["errEdfluxdEL1p5E34"] = (data_ph["errEdfluxdE"]/volume)*Lumi

       data_epem["errdfluxdEL1p5E34"] = (data_epem["errdfluxdE"]/volume)*Lumi
       data_epem["errEdfluxdEL1p5E34"] = (data_epem["errEdfluxdE"]/volume)*Lumi

#       errindx = (["errEdfluxdE","errdfluxdE","errEdfluxdEL1p5E34","errdfluxdEL1p5E34"])
       errindx = (["errdfluxdE","errEdfluxdE"])

       data_neut.to_csv(geometry_location+"/energy_data/energy_"+stationname+"_neutrons.csv",columns = ["e_bin_left","e_bin_right","bin_center","dfluxdEN","EdfluxdE","errdfluxdE"],index=False)
       data_ph.to_csv(geometry_location+"/energy_data/energy_"+stationname+"_photons.csv",columns = ["e_bin_left","e_bin_right","bin_center","dfluxdEN","EdfluxdE","errdfluxdE"],index=False)
       data_epem.to_csv(geometry_location+"/energy_data/energy_"+stationname+"_ep.csv",columns = ["e_bin_left","e_bin_right","bin_center","dfluxdEN","EdfluxdE","errdfluxdE"],index=False)

       if (station=="33" or station=="34" or station=="35"):

           data_neut2["dfluxdEL1p5E34"] = (data_neut2["dfluxdE"]/volume2)*Lumi
           data_neut2["EdfluxdEL1p5E34"] = (data_neut2["EdfluxdE"]/volume2)*Lumi
       
           data_ph2["dfluxdEL1p5E34"] = (data_ph2["dfluxdE"]/volume2)*Lumi
           data_ph2["EdfluxdEL1p5E34"] = (data_ph2["EdfluxdE"]/volume2)*Lumi

           data_epem2["dfluxdEL1p5E34"] = (data_epem2["dfluxdE"]/volume2)*Lumi
           data_epem2["EdfluxdEL1p5E34"] = (data_epem2["EdfluxdE"]/volume2)*Lumi


           data_neut2["errdfluxdEL1p5E34"] = (data_neut2["errdfluxdE"]/volume2)*Lumi
           data_neut2["errEdfluxdEL1p5E34"] = (data_neut2["errEdfluxdE"]/volume2)*Lumi
       
           data_ph2["errdfluxdEL1p5E34"] = (data_ph2["errdfluxdE"]/volume2)*Lumi
           data_ph2["errEdfluxdEL1p5E34"] = (data_ph2["errEdfluxdE"]/volume2)*Lumi

           data_epem2["errdfluxdEL1p5E34"] = (data_epem2["errdfluxdE"]/volume2)*Lumi
           data_epem2["errEdfluxdEL1p5E34"] = (data_epem2["errEdfluxdE"]/volume2)*Lumi

           data_neut2.to_csv(geometry_location+"/energy_data/energy_"+stationname2+"_neutrons.csv",columns = ["e_bin_left","e_bin_right","bin_center","dfluxdEN","EdfluxdE","errdfluxdE"],index=False)
           data_ph2.to_csv(geometry_location+"/energy_data/energy_"+stationname2+"_photons.csv",columns = ["e_bin_left","e_bin_right","bin_center","dfluxdEN","EdfluxdE","errdfluxdE"],index=False)
           data_epem2.to_csv(geometry_location+"/energy_data/energy_"+stationname2+"_ep.csv",columns = ["e_bin_left","e_bin_right","bin_center","dfluxdEN","EdfluxdE","errdfluxdE"],index=False)

        
        
       plt.rcParams.update({'font.size': 11.5})
       
       
       #FLUKA_labels = '\n'.join((
       #   r'CMS-FLUKA studies v3.31.4.2',
       #   r'Geometry v3.31.4.2'))
        
       FLUKA_labels = r'CMS FLUKA simulation v3.31.4.2'
       
       for category, errindx in zip(plot_category,errindx):

       # energy plots
            data_neut.plot(x="bin_center",y=category,yerr=errindx,style='o');plt.xscale('log');plt.yscale('log')
            plt.savefig(geometry_location+"/plots/"+LumiTag+"energy/"+stationname+"_"+category+"_neut.png")

            data_ph.plot(x="bin_center",y=category,yerr=errindx,style='o');plt.xscale('log');plt.yscale('log')
            plt.savefig(geometry_location+"/plots/"+LumiTag+"energy/"+stationname+"_"+category+"_ph.png")

            data_epem.plot(x="bin_center",y=category,yerr=errindx,style='o');plt.xscale('log');plt.yscale('log')
            plt.savefig(geometry_location+"/plots/"+LumiTag+"energy/"+stationname+"_"+category+"_epem.png")



            ax = data_neut.plot(x="bin_center",y=category,yerr=errindx,color='black',marker='o',linestyle='none');plt.xscale('log');plt.yscale('log')
            #if category != "dfluxdEN":
                #ax.set_title("Differential distribution of Fluence in Energy (lethargy) for "+stationname)
            #else: 
                #ax.set_title("Differential distribution of Fluence in Energy for "+stationname)
            data_ph.plot(x="bin_center",y=category,yerr=errindx,color='red',marker='s',fillstyle='none',linestyle='none',ax=ax);plt.xscale('log');plt.yscale('log')
            data_epem.plot(x="bin_center",y=category,yerr=errindx,color='green',marker ='^',fillstyle='none',linestyle='none',ax=ax);plt.xscale('log');plt.yscale('log')
            #ax.set_ylim([10E-9,10E-5])
            ax.set_xlabel("E [GeV]",horizontalalignment='right')
            if category == "dfluxdEN":
                ax.set_ylabel("dFlux/dE [$cm^{-2} primary^{-1} GeV^{-1}$]")
            else: 
                ax.set_ylabel("E(dFlux/dE) [$cm^{-2} primary^{-1} $]")
            if category == "dfluxdEN":
                ax.text(-0.12,-0.11,FLUKA_labels,transform=ax.transAxes, fontsize=10,
                    verticalalignment='top')
            else:
                ax.text(0.04, 0.99, FLUKA_labels, transform=ax.transAxes, fontsize=12,
                    verticalalignment='top')
            font0 = FontProperties()
            font = font0.copy()
            font.set_style('italic')
            ax.text(0.55,1.03,"CMS Simulation",fontproperties=font,transform=ax.transAxes,fontsize=12)       
            ax.tick_params(direction='out', length=6, width=2, grid_alpha=0.5)
            #ax.set_ylim(10E-11,10E-8)
            plt.legend(['Neutrons','Photons','Electrons/Positrons'])
            plt.savefig(geometry_location+"/plots/"+LumiTag+"energy/combined/"+stationname+"_"+category+".png")


            if (station=="33" or station=="34" or station=="35"):

                data_neut2.plot(x="bin_center",y=category,yerr=errindx);plt.xscale('log');plt.yscale('log')
                plt.savefig(geometry_location+"/plots/"+LumiTag+"energy/"+stationname2+"_"+category+"_neut.png")

                data_ph2.plot(x="bin_center",y=category,yerr=errindx);plt.xscale('log');plt.yscale('log')
                plt.savefig(geometry_location+"/plots/"+LumiTag+"energy/"+stationname2+"_"+category+"_ph.png")

                data_epem2.plot(x="bin_center",y=category,yerr=errindx);plt.xscale('log');plt.yscale('log')
                plt.savefig(geometry_location+"/plots/"+LumiTag+"energy/"+stationname2+"_"+category+"_epem.png")

                ax = data_neut2.plot(x="bin_center",y=category,yerr=errindx,color='black',marker='o',linestyle='none');plt.xscale('log');plt.yscale('log')
                ax.set_title("Particle Energy Spectra (lethargy) for "+stationname)
                data_ph2.plot(x="bin_center",y=category,yerr=errindx,color='red',marker='s',fillstyle='none',linestyle='none',ax=ax);plt.xscale('log');plt.yscale('log')
                data_epem2.plot(x="bin_center",y=category,yerr=errindx,color='green',marker ='^',fillstyle='none',linestyle='none',ax=ax);plt.xscale('log');plt.yscale('log')
                #ax.set_ylim([10E-9,10E-5])
                ax.set_xlabel("E [GeV]")
                if category == "dfluxdEN":
                    ax.set_ylabel("dFlux/dE [$cm^{-2} primary^{-1} GeV^{-1}$]")
                else: 
                    ax.set_ylabel("E(dFlux/dE) [$cm^{-2} primary^{-1} $]")
                ax.text(0.04,0.99,FLUKA_labels,transform=ax.transAxes, fontsize=12,
                       verticalalignment='top')
                #ax.set_ylim(10E-11,10E-8)
                plt.legend(['Neutrons','Photons','Electrons/Positrons'])
                plt.savefig(geometry_location+"/plots/"+LumiTag+"energy/combined/"+stationname2+"_"+category+".png")