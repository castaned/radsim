import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import pylab
import sys
from math import sqrt

# change according to directory in gitlab/github 
geometry_location="C:/Users/Usuario/Documents/Github/radsim/geometries/v_3_31_4_2"

# sensitivity estimations for different particles [0]=neutrons,[1]=photons,[2]=electrons/positrons
sens = [0.0026,0.016,0.35]

## Luminosity factor: for 1x10E34 use 800000000, for 5x10E34  use 4000000000
LumiRun2 = 800000000.0*1.5   #  1.5x10E34  (Run-2 muon chambers)

colnames=["bin_left","bin_right","flux","error"]

detectors=["MB1","MB2","MB3","MB4","RE1213","RE2122","RE3233","RE4243","RE1213_neg","RE2122_neg","RE3233_neg","RE4243_neg",
           "RB1top","RB2top","RB3","RB4","ME1213","ME2","ME3","ME4","ME1213_neg","ME2_neg","ME3_neg","ME4_neg"]
 
particles = ["neut","ph","epem"]

colnames2 = ["bin_center","sum_flux"]

dataframe_collection = {}


LumiTag = "1.5x10E34/"
z_region = "R=790 cm "

for det in detectors:
    count_part=0


    for part in particles:

        det_part = pd.read_table(geometry_location+"/raw_data/"+det+"_"+part+".dat",delim_whitespace=True,skiprows=list(range(0,1)),names=colnames)
        det_part["bin_center"] = (det_part["bin_left"]+det_part["bin_right"])/2.

        det_part["flux"]  = det_part["flux"]*LumiRun2
        det_part["hitrate"] = det_part["flux"]*sens[count_part]
        det_part["errflux"] = (det_part["error"]/100.0)*det_part["flux"]
        det_part["errhitrate"] = det_part["errflux"]*sens[count_part]


        
        if det=="MB1": det_part["eta_bin_center"] =  -np.log(np.tan(  (np.pi/4.0)+  (np.arctan( det_part["bin_center"]/425. ))/2.))
        if det=="MB2": det_part["eta_bin_center"] =  -np.log(np.tan(  (np.pi/4.0)+  (np.arctan( det_part["bin_center"]/520. ))/2.))
        if det=="MB3": det_part["eta_bin_center"] =  -np.log(np.tan(  (np.pi/4.0)+  (np.arctan( det_part["bin_center"]/620. ))/2.))
        if det=="MB4": det_part["eta_bin_center"] =  -np.log(np.tan(  (np.pi/4.0)+  (np.arctan( det_part["bin_center"]/720. ))/2.))

        if det=="RB1top": det_part["eta_bin_center"] =  -np.log(np.tan(  (np.pi/4.0)+  (np.arctan( det_part["bin_center"]/450. ))/2.))
        if det=="RB2top": det_part["eta_bin_center"] =  -np.log(np.tan(  (np.pi/4.0)+  (np.arctan( det_part["bin_center"]/520. ))/2.))
        if det=="RB3": det_part["eta_bin_center"] =  -np.log(np.tan(  (np.pi/4.0)+  (np.arctan( det_part["bin_center"]/620. ))/2.))
        if det=="RB4": det_part["eta_bin_center"] =  -np.log(np.tan(  (np.pi/4.0)+  (np.arctan( det_part["bin_center"]/720. ))/2.))

        if det=="RE1213": det_part["eta_bin_center"] = -np.log(np.tan((np.arctan( det_part["bin_center"]/685.))/2.))
        if det=="RE2122": det_part["eta_bin_center"] = -np.log(np.tan((np.arctan( det_part["bin_center"]/790.))/2.))
        if det=="RE3233": det_part["eta_bin_center"] = -np.log(np.tan((np.arctan( det_part["bin_center"]/975.))/2.))
        if det=="RE4243": det_part["eta_bin_center"] = -np.log(np.tan((np.arctan( det_part["bin_center"]/1050.))/2.))

        if det=="ME1213": det_part["eta_bin_center"] = -np.log(np.tan((np.arctan( det_part["bin_center"]/685.))/2.))
        if det=="ME2": det_part["eta_bin_center"] = -np.log(np.tan((np.arctan( det_part["bin_center"]/790.))/2.))
        if det=="ME3": det_part["eta_bin_center"] = -np.log(np.tan((np.arctan( det_part["bin_center"]/975.))/2.))
        if det=="ME4": det_part["eta_bin_center"] = -np.log(np.tan((np.arctan( det_part["bin_center"]/1050.))/2.))

        if det=="ME1213_neg": det_part["eta_bin_center"] = np.log(np.tan((np.arctan( det_part["bin_center"]/685.))/2.))
        if det=="ME2_neg": det_part["eta_bin_center"] = np.log(np.tan((np.arctan( det_part["bin_center"]/790.))/2.))
        if det=="ME3_neg": det_part["eta_bin_center"] = np.log(np.tan((np.arctan( det_part["bin_center"]/975.))/2.))
        if det=="ME4_neg": det_part["eta_bin_center"] = np.log(np.tan((np.arctan( det_part["bin_center"]/1050.))/2.))

        if det=="RE1213_neg": det_part["eta_bin_center"] = np.log(np.tan((np.arctan( det_part["bin_center"]/685.))/2.))
        if det=="RE2122_neg": det_part["eta_bin_center"] = np.log(np.tan((np.arctan( det_part["bin_center"]/790.))/2.))
        if det=="RE3233_neg": det_part["eta_bin_center"] = np.log(np.tan((np.arctan( det_part["bin_center"]/975.))/2.))
        if det=="RE4243_neg": det_part["eta_bin_center"] = np.log(np.tan((np.arctan( det_part["bin_center"]/1050.))/2.))



        if(det=="MB1" or det=="MB2" or det=="MB3" or det=="MB4" or det=="RB1top" or det=="RB2top" or det=="RB3" or det=="RB4"):
            det_part.plot(x="bin_center",y="flux",yerr="errflux",color="red"); plt.savefig(geometry_location+"/plots/"+LumiTag+"fluxes/"+det+"_"+part+"_flux_vs_Z.png")
            det_part.plot(x="bin_center",y="hitrate",yerr="errhitrate",color="red"); plt.savefig(geometry_location+"/plots/"+LumiTag+"hitrate/"+det+"_"+part+"_hitrate_vs_Z.png")
        else:
            det_part.plot(x="bin_center",y="flux",yerr="errflux",color="red"); plt.savefig(geometry_location+"/plots/"+LumiTag+"fluxes/"+det+"_"+part+"_flux_vs_R.png")
            det_part.plot(x="bin_center",y="hitrate",yerr="errhitrate",color="red"); plt.savefig(geometry_location+"/plots/"+LumiTag+"hitrate/"+det+"_"+part+"_hitrate_vs_R.png")


        det_part.plot(x="eta_bin_center",y="flux",yerr="errflux",color="red"); plt.savefig(geometry_location+"/plots/"+LumiTag+"fluxes/"+det+"_"+part+"_flux_vs_eta.png")
        det_part.plot(x="eta_bin_center",y="hitrate",yerr="errhitrate",color="red"); plt.savefig(geometry_location+"/plots/"+LumiTag+"hitrate/"+det+"_"+part+"_hitrate_vs_eta.png")


        count_part=count_part+1

        if part=="neut": temp1 = det_part.copy()
        if part=="ph":   temp2 = det_part.copy()
        if part=="epem": temp3 = det_part.copy()
        
    dataframe_collection[det] =   pd.DataFrame({'eta_bin_center' : temp1['eta_bin_center'],'totflux' : (temp1['flux']+temp2['flux']+temp3['flux']),
                                   'errtotflux' : np.sqrt( temp1["errflux"]*temp1["errflux"] + temp2["errflux"]*temp2["errflux"] + temp3["errflux"]*temp3["errflux"]),
                                   'tothitrate' : temp1['hitrate']+temp2['hitrate']+temp3['hitrate'],
                                   'errtothitrate' : np.sqrt( temp1["errhitrate"]*temp1["errhitrate"] + temp2["errhitrate"]*temp2["errhitrate"] + temp3["errhitrate"]*temp3["errhitrate"])})


    dataframe_collection[det].plot(x="eta_bin_center",y="totflux",yerr="errtotflux",color="red"); plt.savefig(geometry_location+"/plots/"+LumiTag+"fluxes/"+det+"_totflux_vs_eta.png")
    dataframe_collection[det].plot(x="eta_bin_center",y="tothitrate",yerr="errtothitrate",color="red"); plt.savefig(geometry_location+"/plots/"+LumiTag+"hitrate/"+det+"_tothitrate1p5E34_vs_eta.png")
        
#  combined plots DT and RPCs

#for  key in dataframe_collection.keys():
#    print(key)

plt.xlabel(r'$|\eta$|',fontsize=16,horizontalalignment='right',x=1.0)
ax = dataframe_collection["RB1top"].plot(x="eta_bin_center",y="tothitrate",yerr="errtothitrate",color="red")
dataframe_collection["RE1213"].plot(x="eta_bin_center",y="tothitrate",yerr="errtothitrate",color="blue",ax=ax)
dataframe_collection["RE1213_neg"].plot(x="eta_bin_center",y="tothitrate",yerr="errtothitrate",color="blue",ax=ax)
plt.savefig(geometry_location+"/plots/"+LumiTag+"/hitrate/combined/RB1top_vs_RE1213_hitrate_vs_eta.png")



