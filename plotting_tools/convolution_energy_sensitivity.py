import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import pylab
import sys
from math import sqrt
from matplotlib.font_manager import FontProperties

# change according to directory in gitlab/github 
geometry_location="C:/Users/Alfredo/Documents/Github/radsim/geometries/v_3_31_4_2"

colNames = ["E","Sen","Err"]

# Sensitivity files from Geant4 simulation ordered in columns  (Energy, Sensitivity, Error  )
sen_neut = pd.read_csv("C:/Users/Alfredo/Downloads/RE22Neutron.txt", header=None, delim_whitespace=True, names=colNames)
sen_ph =  pd.read_csv("C:/Users/Alfredo/Downloads/RE22Gamma.txt",header=None, delim_whitespace=True, names=colNames)
sen_ep =  pd.read_csv("C:/Users/Alfredo/Downloads/RE22Electron.txt", header = None, delim_whitespace=True, names=colNames)

#print(sen_neut["Sen"]/100.0)

## Luminosity factor: for 1x10E34 use 800000000, for 5x10E34  use 4000000000
LumiRun2 = 800000000.0*1.5   #  1.5x10E34  (Run-2 muon chambers)

colnames=["bin_left","bin_right","flux","error"]

#detectors=["MB1","MB2","MB3","MB4","RE1213","RE2223","RE3233","RE4243","RE1213_neg","RE2223_neg","RE3233_neg","RE4243_neg",
#          "RB1top","RB2top","RB3","RB4","ME1213","ME2","ME3","ME4","ME1213_neg","ME2_neg","ME3_neg","ME4_neg"]

dataframe_collection = {}

# flux value from USRBIN
RE2223_neut_df = pd.read_table(geometry_location+"/raw_data/RE2223_neut.dat",delim_whitespace=True,skiprows=list(range(0,1)),names=colnames)
RE2223_ph_df = pd.read_table(geometry_location+"/raw_data/RE2223_ph.dat",delim_whitespace=True,skiprows=list(range(0,1)),names=colnames)
RE2223_ep_df = pd.read_table(geometry_location+"/raw_data/RE2223_epem.dat",delim_whitespace=True,skiprows=list(range(0,1)),names=colnames)

RE2223_neut_df["fluxN"]=RE2223_neut_df["flux"]*LumiRun2
RE2223_neut_df["bin_center"]=(RE2223_neut_df["bin_left"]+RE2223_neut_df["bin_right"])/2.

RE2223_ph_df["fluxN"]=RE2223_ph_df["flux"]*LumiRun2
RE2223_ph_df["bin_center"]=(RE2223_ph_df["bin_left"]+RE2223_ph_df["bin_right"])/2.

RE2223_ep_df["fluxN"]=RE2223_ep_df["flux"]*LumiRun2
RE2223_ep_df["bin_center"]=(RE2223_ep_df["bin_left"]+RE2223_ep_df["bin_right"])/2.


# Energy spectra from USRTRACK
data_neut = pd.read_csv(geometry_location+"/energy_data/energy_ME22_neutrons.csv", header=0,names = ["e_bin_left","e_bin_right","bin_center","dfluxdEN","EdfluxdE","errdfluxdE"])
data_ph = pd.read_csv(geometry_location+"/energy_data/energy_ME22_photons.csv", header=0,names = ["e_bin_left","e_bin_right","bin_center","dfluxdEN","EdfluxdE","errdfluxdE"])
data_ep = pd.read_csv(geometry_location+"/energy_data/energy_ME22_ep.csv", header=0,names = ["e_bin_left","e_bin_right","bin_center","dfluxdEN","EdfluxdE","errdfluxdE"])

## Function to obtain the integral from any energy distribution
def integral_fun(df):
      integrated = 0
      for row in df.itertuples():
        deltaE_temp = (row.e_bin_right - row.e_bin_left)
        dEdfluxdE_temp = deltaE_temp*row.dfluxdEN
        integrated +=dEdfluxdE_temp
 #     print("integrated flux: ", integrated)
      return integrated

integrated_in_neut = integral_fun(data_neut) 
integrated_in_ph = integral_fun(data_ph) 
integrated_in_ep = integral_fun(data_ep) 

#print(integrated_in_neut)

R_value_neut = np.array([])
eta_value_neut = np.array([])
hitrate_neut = np.array([])

for row in RE2223_neut_df.itertuples():
    facnorm = row.fluxN/integrated_in_neut
    convolution_collection = pd.DataFrame({"e_bin_right":data_neut["e_bin_right"],"e_bin_left":data_neut["e_bin_left"],"dfluxdEN":(data_neut["dfluxdEN"]*(sen_neut["Sen"]/100.0)*facnorm)})
    hit_rate = integral_fun(convolution_collection)
    eta_temp = -np.log(np.tan((np.arctan( row.bin_center/790.))/2.))
    eta_value_neut = np.append(eta_value_neut,eta_temp)
    R_value_neut = np.append(R_value_neut,row.bin_center)
    hitrate_neut = np.append(hitrate_neut,hit_rate)

print(hitrate_neut)

eta_value_ph = np.array([])
R_value_ph = np.array([])
hitrate_ph = np.array([])

for row in RE2223_ph_df.itertuples():
    facnorm = row.fluxN/integrated_in_ph
    convolution_collection = pd.DataFrame({"e_bin_right":data_ph["e_bin_right"],"e_bin_left":data_ph["e_bin_left"],"dfluxdEN":(data_ph["dfluxdEN"]*(sen_ph["Sen"]/100.0)*facnorm)})
    hit_rate = integral_fun(convolution_collection)
    eta_temp = -np.log(np.tan((np.arctan( row.bin_center/790.))/2.))
    eta_value_ph = np.append(eta_value_ph,eta_temp)
    R_value_ph = np.append(R_value_ph,row.bin_center)
    hitrate_ph = np.append(hitrate_ph,hit_rate)

eta_value_ep = np.array([])
R_value_ep = np.array([])
hitrate_ep = np.array([])

for row in RE2223_ep_df.itertuples():
    facnorm = row.fluxN/integrated_in_ep
    convolution_collection = pd.DataFrame({"e_bin_right":data_ep["e_bin_right"],"e_bin_left":data_ep["e_bin_left"],"dfluxdEN":(data_ep["dfluxdEN"]*(sen_ep["Sen"]/100.0)*facnorm)})
    hit_rate = integral_fun(convolution_collection)
    eta_temp = -np.log(np.tan((np.arctan( row.bin_center/790.))/2.))
    eta_value_ep = np.append(eta_value_ep,eta_temp) 
    R_value_ep = np.append(R_value_ep,row.bin_center)
    hitrate_ep = np.append(hitrate_ep,hit_rate)

# #ax.set_xlabel(r'R [cm]',horizontalalignment='right')
# #ax.set_ylabel(" Particle flux [$cm^{-2} s^{-1}$]")

# #plt.savefig(geometry_location+"/plots/"+LumiTag+"energy/combined/RE2223_energy_per_fluxinR.png")

plt.rcParams.update({'font.size': 12})
 #plt.figure()

 #plt.plot(R_value_ph,hitrate_ph,"salmon")
 #plt.show()

fig, ax = plt.subplots(figsize=(6, 5))
fig.subplots_adjust(bottom=0.15, left=0.2)

FLUKA_labels = '\n'.join((
           r'CMS-FLUKA studies',
           r'Geometry v3.31.4.2'))

# # label_one = r"CMS-FLUKA/Geant4 studies"
label_two = r"L=$1.5x10^{34}$ $cm^{-2}s^{-1}$ (13 TeV)"

R_values = [355.8,407.8,466.5,533.6,602.8,667.6]
rates_data_R = [29.81,16.775,7.165,4.33,4.02,4.53]

ax.errorbar(R_value_neut,hitrate_neut,yerr=hitrate_neut*0.112,label="neutrons",marker="o",color="salmon")
ax.errorbar(R_value_ph,hitrate_ph,yerr=hitrate_ph*0.128,label="photons",marker="o",color="navy")
ax.errorbar(R_value_ep,hitrate_ep,yerr=hitrate_ep*0.38,label="$e^{\pm}$",marker="o",color="green")
ax.errorbar(R_value_ep,hitrate_neut+hitrate_ph+hitrate_ep,yerr=np.sqrt((hitrate_neut*0.112*hitrate_neut*0.112)+(hitrate_ph*0.128*hitrate_ph*0.128)+(hitrate_ep*0.38*hitrate_ep*0.38)),label="Total (neutrons+photons+$e^{\pm}$)",marker="o",color="black")


#ax.plot(R_value_neut,hitrate_neut,"salmon",
#          R_value_ph, hitrate_ph, "navy",
#          R_value_ep, hitrate_ep, "green",
#          R_value_ep,hitrate_neut+hitrate_ph+hitrate_ep,"black")
#ax.plot(R_values,rates_data_R,"black",marker="o")
ax.set_ylabel("Hit Rate [Hz/$cm^{2}$]")
ax.set_xlabel("R [cm]")
ax.set_ylim(top=35.0)
font0 = FontProperties()
font = font0.copy()
font.set_style('italic')
ax.text(0.70,0.93,"780<z<790cm",fontproperties=font,transform=ax.transAxes,fontsize=10)
ax.text(0.01,1.02,"CMS Simulation Preliminary",fontproperties=font,transform=ax.transAxes,fontsize=10)
#ax.text(0.65,1.03,"instL = $1.5x10^34} cm^{-2} s^{-1}$ (13 TeV)",fontproperties=font,transform=ax.transAxes,fontsize=11) 
#ax.text(0.04,1.05,label_one,transform=ax.transAxes,fontsize=10,verticalalignment='top')
ax.text(0.5,1.05,label_two,transform=ax.transAxes,fontsize=10,verticalalignment='top')
 #ax.text(0.4,0.6,FLUKA_labels,transform=ax.transAxes, fontsize=11,
 #                    verticalalignment='top')
leg = ax.legend(fontsize=10)
#plt.legend(("neutrons","photons","$e^{\pm}$","Total (neutrons+photons+$e^{\pm}$)","RPC data (2018)"),fontsize=10)
plt.savefig('C:/Users/Alfredo/Pictures/HitRate_vs_R_sim.png', dpi = 600)


plt.show()

fig, ax = plt.subplots(figsize=(6, 5))
fig.subplots_adjust(bottom=0.15, left=0.2)

# # label_three = r"L=1.5x10E34 $cm^{-2} s^{-1}$"

ax.errorbar(eta_value_neut,hitrate_neut,yerr=hitrate_neut*0.112,label="neutrons",marker="o",color="salmon")
ax.errorbar(eta_value_ph,hitrate_ph,yerr=hitrate_ph*0.128,label="photons",marker="o",color="navy")
ax.errorbar(eta_value_ep,hitrate_ep,yerr=hitrate_ep*0.38,label="$e^{\pm}$",marker="o",color="green")
ax.errorbar(eta_value_ep,hitrate_neut+hitrate_ph+hitrate_ep,yerr=np.sqrt((hitrate_neut*0.112*hitrate_neut*0.112)+(hitrate_ph*0.128*hitrate_ph*0.128)+(hitrate_ep*0.38*hitrate_ep*0.38)),label="Total (neutrons+photons+$e^{\pm}$)",marker="o",color="black")


#ax.plot(eta_value_neut,hitrate_neut,"salmon",
#         eta_value_ph, hitrate_ph, "navy", 
#         eta_value_ep, hitrate_ep, "green",
#         eta_value_ep,  hitrate_neut+hitrate_ph+hitrate_ep,"black")
ax.set_ylabel("Hit Rate [Hz/$cm^{2}$]")
ax.set_xlabel("|$\eta$|")
ax.set_ylim(top=35.0)
#font0 = FontProperties()
# #font = font0.copy()
# # font.set_style('italic')
ax.text(0.06,0.9,"780<z<790cm",fontproperties=font,transform=ax.transAxes,fontsize=10)
ax.text(0.01,1.02,"CMS Simulation Preliminary",fontproperties=font,transform=ax.transAxes,fontsize=10)
##ax.text(0.65,1.03,"instL = $1.5x10^34} cm^{-2} s^{-1}$ (13 TeV)",fontproperties=font,transform=ax.transAxes,fontsize=11 
# # #ax.text(0.04,1.05,label_one,transform=ax.transAxes,fontsize=10,verticalalignment='top')
ax.text(0.5,1.05,label_two,transform=ax.transAxes,fontsize=10,verticalalignment='top')
leg = ax.legend(fontsize=10)
# # plt.legend(("neutrons","photons","$e^{\pm}$","Total (neutrons+photons+$e^{\pm}$)","RPC data (2018)"),fontsize=10)
# # #plt.legend(("n's","$\gamma$'s","$e^{\pm}$","Total (neut+$\gamma$'s+$e^{\pm}$)","RPC data (2018)"),fontsize=10)
# # #plt.legend(("neutrons","photons","Electrons/Positrons","Total","RPC data (2018)"),fontsize=10)

plt.savefig('C:/Users/Alfredo/Pictures/HitRate_vs_eta_sim.png', dpi = 600)

plt.show()

# # #eta_values_data = [1.25092,1.36161,1.53501,0.940752,1.01269,1.104428]
# # #rates_data = [15.55,26.64,44.25,8.96,5.485,6.58]

# # #eta_values_data=[1.30087,1.418788,1.54149,1.006991,1.088097,1.186697]
# # #rates_data = [7.165,16.775,29.81,4.53,4.02,4.33]

eta_values_data= [1.30087,1.418788,1.54149,1.006991,1.088097,1.186697]
rates_data =  [7.165,16.775,29.81,4.53,4.02,4.33]



fig, ax = plt.subplots(figsize=(6, 5))
fig.subplots_adjust(bottom=0.15, left=0.2)

ax.plot(eta_values_data,rates_data,linestyle="none",marker="o",label="RPC data (2018)")
ax.errorbar(eta_value_neut,(hitrate_neut+hitrate_ph+hitrate_ep),yerr=np.sqrt((hitrate_neut*0.112*hitrate_neut*0.112)+(hitrate_ph*0.128*hitrate_ph*0.128)+(hitrate_ep*0.38*hitrate_ep*0.38)),label="Simulation (neutrons+photons+$e^{\pm}$)",marker="o",color="black")
ax.set_ylabel("Hit Rate [Hz/$cm^{2}$]")
ax.set_xlabel("|$\eta$|")
#ax.text(0.05,1.05,label_three,transform=ax.transAxes,fontsize=11,verticalalignment='top')
#ax.text(0.6,1.05,label_three,transform=ax.transAxes,fontsize=11,verticalalignment='top')
#ax.text(0.6,0.75,"780<z<790cm",fontproperties=font,transform=ax.transAxes,fontsize=10)
ax.text(0.02,1.02,"CMS preliminary",fontproperties=font,transform=ax.transAxes,fontsize=10)
#ax.text(0.65,1.03,"instL = $1.5x10^34} cm^{-2} s^{-1}$ (13 TeV)",fontproperties=font,transform=ax.transAxes,fontsize=11 
#ax.text(0.04,1.05,label_one,transform=ax.transAxes,fontsize=10,verticalalignment='top')
ax.text(0.5,1.05,label_two,transform=ax.transAxes,fontsize=10,verticalalignment='top')

leg = ax.legend(fontsize=10)
ax.set_ylim(0.0,37.0)

plt.grid(True, lw = 1, ls = '--', c = '.75')
plt.savefig('C:/Users/Alfredo/Pictures/HitRate_vs_eta_data.png', dpi = 600)

plt.show()

# #print(sen_neut["Sen"])
# #print(hitrate_neut)

fig, ax = plt.subplots(figsize=(6, 5))
fig.subplots_adjust(bottom=0.15, left=0.2)

ax.plot(R_values,rates_data_R,linestyle="none",marker="o",label="RPC data (2018)")
ax.errorbar(R_value_neut,(hitrate_neut+hitrate_ph+hitrate_ep),yerr=np.sqrt((hitrate_neut*0.112*hitrate_neut*0.112)+(hitrate_ph*0.128*hitrate_ph*0.128)+(hitrate_ep*0.38*hitrate_ep*0.38)),label="Simulation (neutrons+photons+$e^{\pm}$)",marker="o",color="black")
#ax.plot(R_value_neut,  hitrate_neut+hitrate_ph+hitrate_ep,"black",label="Simulation (neutrons+photons+$e^{\pm}$)")
ax.set_ylabel("Hit Rate [Hz/$cm^{2}$]")
ax.set_xlabel("R [cm]")
#ax.text(0.05,1.05,label_three,transform=ax.transAxes,fontsize=11,verticalalignment='top')
#ax.text(0.6,1.05,label_three,transform=ax.transAxes,fontsize=11,verticalalignment='top')
#ax.text(0.6,0.75,"780<z<790cm",fontproperties=font,transform=ax.transAxes,fontsize=10)
ax.text(0.02,1.02,"CMS preliminary",fontproperties=font,transform=ax.transAxes,fontsize=10)
#ax.text(0.65,1.03,"instL = $1.5x10^34} cm^{-2} s^{-1}$ (13 TeV)",fontproperties=font,transform=ax.transAxes,fontsize=11 
#ax.text(0.04,1.05,label_one,transform=ax.transAxes,fontsize=10,verticalalignment='top')
ax.text(0.5,1.05,label_two,transform=ax.transAxes,fontsize=10,verticalalignment='top')

leg = ax.legend(fontsize=10)
ax.set_ylim(0.0,37.0)

plt.grid(True, lw = 1, ls = '--', c = '.75')
plt.savefig('C:/Users/Alfredo/Pictures/HitRate_vs_R_data.png', dpi = 600)

plt.show()

