# RadSim

## Description
Collection of plotting scripts and raw data from the FLUKA simulation for the CMS detector, with focus on the 
CMS muon system, the data presented corresponds to different detector configurations (Run-2,Phase-2) and different 
luminosity scenarios.

Relevant information for users is stored in the geometry folder which contains plots for fluxes and hitrates 
for the different muon stations, which are obtained with macros stored in plotting_tools folder

For instance, the recommended geometry for Run-2 is v_3_31_4_2 and the information is distrbuted as follows:

- geometries
    - v_3_31_4_2   (recommended geometry for Run-2 studies)
	    - plots  (plots for energy, fluxes, hitrate for different muon stations)
			-	1.5x10E34  (luminosity used for Run-2 data comparisons)
				- energy
				- fluxes
				- hitrate
				- combined (several muon stations in one canvas)

		- energy_data (energy spectra data files)
		   The energy spectra energy is saved into table format,  different files (per muon station), in the following format:

		  - |  energy_bin_left  |  energy_bin_right   | energ_bin_center    |  dflux/dE   |  EdfluxdE       |   dfluxdEL1p5E34  |  EdfluxdEL1p5E34 

				 where the flux is in fluka units, in order to normalize to luminosity, each value has to be multiplied by the luminosity factor and divided by the volume of the region 
				 for instance  dfluxdEL1p5E34  is the energy spectra bins normalized to the luminosity 1.5x10E34 

		- raw_data (FLUKA simulation output files)